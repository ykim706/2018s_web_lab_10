package ictgradschool.web.lab10.exercise05;

public class User {
    String firstName;
    String lastName;
    String city;
    String country;

    public User(String firstName, String lastName, String city,
            String country){
        this.firstName = firstName;
        this.lastName = lastName;
        this.city = city;
        this.country = country;

    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }
}
