package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.exercise05.User;
import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;

public class UserDetailsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));

        out.println("<a href=\"index.html\">HOME</a><br>");


        out.println("<h3>Data entered: </h3>");
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes



        HttpSession newSession = request.getSession(true);

        String fname = request.getParameter("fname");
        String lname = request.getParameter("lname");
        String city = request.getParameter("city");
        String country = request.getParameter("country");

        User user = new User(fname, lname, city, country);
        newSession.setAttribute( "user", user);

        Cookie c = new Cookie("fname", fname);
        Cookie d = new Cookie("lname", lname);
        Cookie e = new Cookie("city", city);
        Cookie f = new Cookie("country", country);
        response.addCookie(c);
        response.addCookie(d);
        response.addCookie(e);
        response.addCookie(f);

        out.println("<ul>");
        out.println("<li>First Name: "+ fname + "</li>" );

        out.println("<li>Last Name: " + lname + "</li>");

        out.println("<li>Country: " + country + "</li>");
        out.println("<li>City: " + city + "</li>");
        out.println("</ul>");

        out.println(HtmlHelper.getHtmlPageFooter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Process POST requests the same as GET requests
        doGet(request, response);
    }
}
