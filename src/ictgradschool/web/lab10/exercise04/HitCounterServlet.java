package ictgradschool.web.lab10.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        Cookie[] cookies = req.getCookies();
        Cookie cookie = new Cookie("hits", "0" );

        //get hits cookie
        if (cookies != null) {
            for (Cookie cookie1 : cookies) {
                if (cookie1.getName().equals("hits")) {
                    cookie = cookie1;
                }
            }
        }

        if ("true".equalsIgnoreCase(req.getParameter("removeCookie"))) {
            cookie.setMaxAge(0);


            //TODO - add code here to delete the 'hits' cookie

        } else {

            cookie.setValue(Integer.parseInt(cookie.getValue()) + 1 + "");

        }

        //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie


        resp.addCookie(cookie);
        resp.sendRedirect("hit-counter.html");
        //TODO - use the response object's send redirect method to refresh the page
    }
}
